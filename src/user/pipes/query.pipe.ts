import { Injectable, PipeTransform } from '@nestjs/common';
import { TFilters, TQuery } from '../helper/types';

@Injectable()
export class QueryPipe implements PipeTransform {
  transform(value: TQuery): TFilters {
    const { page, limit, sort, search, ...rest } = value;

    const obj: TFilters = {
      filter: {
        ...rest,
      },
      page: +page ?? 1,
      limit: +limit ?? 10,
      search: search ?? '',
      sort: sort ?? '',
    };

    return obj;
  }
}
