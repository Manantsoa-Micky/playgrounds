import { Controller, Post, Body, Get, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateEnterpriseDto } from './dto/create-enterprise.dto';
import { TFilters, TQueryFilters } from './helper/types';
import { QueryPipe } from './pipes/query.pipe';

@Controller('user')
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/create-user')
  async createUser(@Body() data: CreateUserDto) {
    return this.userService.createUser(data);
  }

  @Post('/create-enterprise')
  async createEnterprise(@Body() data: CreateEnterpriseDto) {
    return this.userService.createEnterprise(data);
  }

  @Get('/get-all')
  async getAllUsers() {
    return await this.userService.getAllUsers();
  }

  @Get('/get-infos')
  async getInfos(@Query(QueryPipe) query: TFilters) {
    const _query: TQueryFilters = {
      filter: {
        ...query.filter,
      },
      populate: {
        collectionName: 'enterprises',
        localField: 'enterprise',
        foreignField: '_id',
        as: 'enterprise',
        populate: {
          collectionName: 'adresses',
          localField: 'adress',
          foreignField: '_id',
          as: 'adress',
        },
      },
      page: query.page,
      limit: query.limit,
    };

    const result = await this.userService.getInfos(_query);
    return result;
  }
}
