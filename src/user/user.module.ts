import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './entities/user.entity';
import { Enterprise, EnterpriseSchema } from './entities/enterprise';
import { Adress, AdressSchema } from './entities/location';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([
      { name: Enterprise.name, schema: EnterpriseSchema },
    ]),
    MongooseModule.forFeature([{ name: Adress.name, schema: AdressSchema }]),
    // MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    // MongooseModule.forFeatureAsync([
    //   {
    //     name: User.name,
    //     useFactory: () => {
    //       const schema = UserSchema;
    //       schema.pre('findOneAndDelete', async function (next) {
    //         const userToDelete = await this.model.findOne(this.getQuery());
    //         await PostModel.deleteMany({ _id: { $in: userToDelete.reviews } });
    //         next();
    //       });
    //       return schema;
    //     },
    //   },
    // ]),
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
