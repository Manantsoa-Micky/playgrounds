import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { Aggregate, Model } from 'mongoose';
import { User } from './entities/user.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Enterprise } from './entities/enterprise';
import { Adress } from './entities/location';
import { CreateEnterpriseDto } from './dto/create-enterprise.dto';
import { TQueryFilters } from './helper/types';
import { buildPipeline } from './helper/pipelineBuilder';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    @InjectModel(Enterprise.name)
    private readonly enterpriseModel: Model<Enterprise>,
    @InjectModel(Adress.name) private readonly adressModel: Model<Adress>,
  ) {}

  async createUser(userData: CreateUserDto) {
    const enterprise = await this.enterpriseModel.findOne({
      name: userData.enterprise,
    });
    const newUser = new this.userModel(userData);
    newUser.enterprise = enterprise;
    await newUser.save();
    return newUser;
  }

  async createEnterprise(data: CreateEnterpriseDto) {
    const newEnterprise = new this.enterpriseModel(data);
    const newAdress = new this.adressModel(data.adress);
    await newAdress.save();
    newEnterprise.adress = newAdress;
    await newEnterprise.save();
    return newEnterprise;
  }

  async getAllUsers() {
    return await this.userModel.aggregate([
      {
        $match: {
          firstname: { $regex: 'mi', $options: 'i' },
        },
      },
    ]);
  }

  async getInfos(query: TQueryFilters): Promise<Aggregate<User[]>> {
    const populate = buildPipeline(query);
    return await this.userModel.aggregate(populate);
  }
}
