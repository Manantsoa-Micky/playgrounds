export type TQueryOptions = {
  filter?: {
    [key: string]: string;
  };
  populate?: {
    collectionName: string;
    localField: string;
    foreignField: string;
    as: string;
    populate?: IPopulate;
  };
  search?: string;
  page?: number;
  limit?: number;
};

export interface IPopulate {
  collectionName: string;
  localField: string;
  foreignField: string;
  as: string;
  populate?: IPopulate;
}

export type TQuery = {
  page: number;
  limit: number;
  search: string;
  sort: string;
};

export type TFilters = {
  filter: Record<string, any>;
  page: number;
  limit: number;
  search: string;
  sort: string;
};
export type TQueryFilters = {
  filter?: Record<string, any>;
  populate?: {
    collectionName: string;
    localField: string;
    foreignField: string;
    as: string;
    populate?: IPopulate;
  };
  search?: {
    searchString: string;
    fieldsToSearch: string[];
  };
  projection?: string[];
  page?: number;
  limit?: number;
};
