import { buildLookupStages, matchbuilder, projectionBuilder } from './helpers';
import { TQueryFilters } from './types';

export function buildPipeline(query: TQueryFilters) {
  const stages: any[] = [];

  if (query.populate) {
    stages.push(...buildLookupStages(query.populate));
  }

  if (query.filter.isNotEmpty) {
    stages.push(
      matchbuilder(
        query.filter,
        query.search?.fieldsToSearch,
        query.search?.searchString,
      ),
    );
  }

  if (query.projection) {
    stages.push({ $project: projectionBuilder(query.projection) });
  }

  stages.push({
    $facet: {
      data: [
        { $skip: (query.page - 1) * query.limit },
        { $limit: query.limit },
      ],
      metadata: [{ $count: 'totalItems' }],
    },
  });

  stages.push({
    $project: {
      data: 1,
      metadata: {
        test1: { $arrayElemAt: ['$metadata', 0] },
        itemCount: { $size: '$data' },
        page: `${query.page}`,
        limit: `${query.limit}`,
      },
    },
  });

  return stages;
}
