import { IPopulate } from './types';

export function matchbuilder(
  filters: Record<string, any> | undefined,
  searchFields?: string[] | undefined,
  searchString?: string | undefined,
): object {
  const match: any = { $and: [] };
  if (searchFields && searchString) {
    match.$and.push({
      $or: searchFields.map((field) => ({
        [field]: { $regex: searchString, $options: 'i' },
      })),
    });
  }
  if (filters) {
    for (const filter in filters) {
      match.$and.push({
        [filter]: { $regex: filters[filter], $options: 'i' },
      });
    }
  }
  return { $match: match };
}
export function buildLookupStages(populate: IPopulate): any[] {
  const stages: any[] = [];

  function addLookupStage(population: IPopulate) {
    stages.push({
      $lookup: {
        from: population.collectionName,
        localField: population.localField,
        foreignField: population.foreignField,
        as: population.as,
        ...(population.populate && {
          pipeline: buildLookupStages(population.populate),
        }),
      },
    });
    stages.push({
      $unwind: `$${population.as}`,
    });
  }

  addLookupStage(populate);

  return stages;
}
export function projectionBuilder(list: string[]) {
  const obj = {};
  list.forEach((key) => {
    obj[key] = 1;
  });

  return obj;
}
