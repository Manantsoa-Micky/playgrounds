import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Schema({ timestamps: true })
export class Adress {
  @Prop({ type: String, required: true })
  country: string;

  @Prop({ type: String, required: true })
  city: string;

  @Prop({ type: String, required: true })
  street: string;

  @Prop({ type: Number, required: true })
  zipCode: number;
}

export const AdressSchema = SchemaFactory.createForClass(Adress);

export const AdressModel = mongoose.model('Adress', AdressSchema);

AdressSchema.index({ country: 'text' });
AdressSchema.index({ city: 'text' });
AdressSchema.index({ street: 'text' });
