import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Enterprise } from './enterprise';

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ type: String, required: true })
  firstname: string;

  @Prop({ type: String, required: true })
  lastname: string;

  @Prop({ type: Date, required: true })
  birthdate: Date;

  @Prop({ type: Number, required: true })
  salary: number;

  @Prop({ type: String, required: true })
  position: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Enterprise' })
  enterprise: Enterprise;
}

export const UserSchema = SchemaFactory.createForClass(User);

export const UserModel = mongoose.model('User', UserSchema);

UserSchema.index({ firstname: 'text' });
UserSchema.index({ lastname: 'text' });
