import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Adress } from './location';

@Schema({ timestamps: true })
export class Enterprise {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Number, required: true })
  capital: number;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Location' })
  adress: Adress;

  @Prop({ type: String, required: true })
  type: string;
}

export const EnterpriseSchema = SchemaFactory.createForClass(Enterprise);

export const EnterpriseModel = mongoose.model('Enterprise', EnterpriseSchema);

EnterpriseSchema.index({ name: 'text' });
EnterpriseSchema.index({ type: 'text' });
