import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { Adress } from '../entities/location';
import { CreateAdressDto } from './create-adress.dto';

export class CreateEnterpriseDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  capital: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  type: string;

  @ApiProperty({ type: () => CreateAdressDto })
  @Type(() => CreateAdressDto)
  @ValidateNested()
  adress: Adress;
}
