import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  firstname: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  lastname: string;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  birthdate: Date;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  salary: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  enterprise: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  position: string;
}
